#!/bin/bash

# This script enables gossip.py to run as a systemd service

# Prompt for the user who will run the script
echo "Choose the user who will run the script:"
read username

# Create the service
echo "[Unit]
Description=Gossip Service
After=multi-user.target

[Install]
WantedBy=multi-user.target

[Service]
Type=simple
User=$username
WorkingDirectory=$PWD
ExecStart=/usr/bin/python3 $PWD/gossip.py
Restart=on-failure
RestartSec=30" >> /etc/systemd/system/gossip.service

# Reload systemd
systemctl daemon-reload

# Enable and start service
systemctl enable gossip