# Gossip

Gossip is a free Python script that will monitor a list of systemd services, send a message to a Slack channel on fail and attempt restart.

It was inspired by @glennzw's [DevOops](https://github.com/glennzw/DevOops).

![Example](example.png)

## Requirements

* Python 3.6+.
* SSH access and sudo privileges on the server that you want to monitor.
* Slack account with administration privileges.

## Generate a Slack API token

1. Create a [new Slack App](https://api.slack.com/apps/).
2. Go to **OAuth & Permissions** and add a `chat:write` scope to your app.
3. [Add the app to your workspace](https://slack.com/help/articles/202035138-Add-an-app-to-your-workspace).
4. Add the app to the channel where you want to receive the messages.
5. Go back to OAuth & Permissions and copy the **Bot User OAuth Access Token**.

## Download the script

Clone this repo in the server that you want to monitor:

```shell script
git clone https://gitlab.com/guillearch/gossip.git
```

## Install the dependencies

Move to the folder of the repo and type:

```shell script
pip install -r requirements.txt
```

## Edit the config file

Edit `config.ini` according to your needs:

```ini
[Slack]
# Bot User OAuth Access Token
Token: xoxb-333649436676-799261852869-clFJVVIaoJahpORboa3Ba2al
# Name of the channel
Channel: python-messages
# ID of the users to mention if human intervention is needed, separated by comma
Users: <@CB500X>,<@F850GS>

[Services]
# Name of the systemd services to be watched, separated by comma
Services: mysql,nginx
# Times the script will attempt to restart a service before giving up
Attempts: 3
```

## Edit the sudoers file

Gossip will run the **sudo** command every time it tries to restart a service, which normally would prompt the user for their password.

That would be very inconvenient, so before continuing [edit the sudoers file](https://www.cyberciti.biz/faq/linux-unix-running-sudo-command-without-a-password/) to allow your user to run `sudo systemctl restart <service>` without a password.

For instance, if you are monitoring Nginx and MySQL, you would append this line to `/etc/sudoers`:

```
<your-user>    ALL = NOPASSWD:     /bin/systemctl restart nginx, /bin/systemctl restart mysql
```

## Run Gossip!

Gossip is bundled with a script that enables it to run as a **systemd service**. Type:

```shell script
sudo chmod +x enable-gossip.sh && sudo ./enable-gossip.sh
``` 

This script will prompt you for the user who will run the script. This user must be the same specified in the sudoers file.

Now you can use `systemctl` to run, monitor and stop Gossip:

```
# Start Gossip
sudo systemctl start gossip
# Check if Gossip is running
sudo systemctl status gossip
# Stop Gossip
sudo stop systemctl stop gossip
```

If you don't want to enable it as a systemd service, you can run it directly:

```
sudo python3 gossip.py &
```