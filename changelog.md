# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# [0.1.2] - 2020-04-28

- Added prompt to request user in enable-gossip.sh and updated README.md.

# [0.1.1] - 2020-04-27

- Fixed a bug in enable-gossip.sh and typos in README.md.

## [0.1.0] - 2020-04-24

- First release! :tada: