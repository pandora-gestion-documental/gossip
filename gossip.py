from configparser import ConfigParser
from subprocess import Popen, PIPE
import time
import psutil
import slack


# Read config.ini
class Configuration:
    config = ConfigParser()
    config.read('config.ini')
    token = config.get('Slack', 'Token')
    channel = config.get('Slack', 'Channel')
    users = ''.join(config.get('Slack', 'Users')).replace(',', ' ')
    services = ''.join(config.get('Services', 'Services').split()).split(',')
    attempts = int(config.get('Services', 'Attempts'))


configuration = Configuration()
black_list = []


def main():
    while True:
        ram = psutil.virtual_memory().percent
        cpu = psutil.cpu_percent()
        services_watched = watch_services()

        for key, value in services_watched.items():
            if key in black_list:
                update_black_list(key)
                continue
            if value != 0:
                send_message(f':large_blue_circle: Dear Human,'
                             f'service *{key}* has stopped! '
                             f'CPU load: {cpu}%. RAM load: {ram}%.')
                restart(key)

        time.sleep(30)


# Check the status of a service
def return_code(command):
    process = Popen(command, stderr=PIPE, stdin=PIPE, stdout=PIPE, shell=True)
    process.communicate()
    return process.returncode


# Create a dictionary with the services and their status
def watch_services():
    watched = {}
    for service in configuration.services:
        command = f'systemctl status {service}'
        status = return_code(command)
        if status == 4:
            exit(f'Service {service} is not enabled!')
        watched.update({service: status})
    return watched


# Remove a blacklisted service if it is running again
def update_black_list(unit):
    command = f'systemctl status {unit}'
    if return_code(command) == 0:
        black_list.remove(unit)


# Send message to the Slack channel
def send_message(message):
    client = slack.WebClient(configuration.token)
    client.chat_postMessage(channel=configuration.channel, text=message)


# Try to restart the service. Don't forget to edit /etc/sudoers:
# sudo visudo
# <user>    ALL = NOPASSWD:     /bin/systemctl restart <service>
# https://www.cyberciti.biz/faq/linux-unix-running-sudo-command-without-a-password/
def restart(unit):
    command = f'sudo systemctl restart {unit}'  # sudo needs password
    send_message('Attempting restart...')
    for i in range(configuration.attempts):
        if return_code(command) == 0:
            send_message(f':white_circle: Successfully restarted *{unit}*! '
                         f'You owe me. Love, your Server.')
            break
        else:
            send_message(f'Failed to restart {unit}.')
            # Stop to try when reached the number of attempts defined in
            # config.ini and blacklist the service to ensure that no more
            # restarts are attempted in the next iteration of main loop
            if i + 1 == configuration.attempts:
                send_message(f':red_circle: {configuration.users} '
                             f'Restart failed {i + 1} times. No more '
                             f'restarts will be attempted. Please log in to '
                             f'the server and restart *{unit}* manually. '
                             f'I am so sorry. Love, your Server.')
                black_list.append(unit)


main()
